import 'package:flutter/material.dart';
import 'package:news_app/response_article.dart';

class DetailNewsPage extends StatelessWidget {
  final Article article;
  const DetailNewsPage({super.key, required this.article});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 20),
        children: [
          Image.network(
            article.urlToImage,
          ),
          Text(article.description)
        ],
      ),
    );
  }
}
